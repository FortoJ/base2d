extends Node




var gui

var state = null setget set_state
var state_old = null

var state_db = {}
onready var parent = get_parent()

#initiate machine
func _add_state(state_add):
	state_db[state_add] = get_node(state_add)
	
func _enter_tree():
	gui = get_parent().get_node("gui")
# warning-ignore:return_value_discarded
	owner.get_node("sprite").connect("animation_finished",self,"anim_finished")
	
func _ready():
	for i in get_children():
		_add_state(i.name)
	call_deferred("set_state","idle")


func _input(event):
	if state != null:
		state_db[state].handle_input(event)


func _process(delta):
	

	if state != null:state_db[state].logic(delta)

func anim_finished():
#	if owner.anim == "idle2walk":
#		owner.anim_new = "walk"
	if state != null:state_db[state].anim_finish(owner.anim)


#switcher
func set_state(new_state):
	if state == new_state:return
	state_old = state
	state = new_state

	if ![state_old,state].has(null):
		state_db[state].entering(state_old)
		state_db[state_old].exiting(state)




#mechanics helper
