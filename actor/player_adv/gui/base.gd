#extends CanvasLayer
extends Node2D

var screen_size = Vector2()
onready var cursor = $cursor

func _enter_tree():
	get_tree().connect("screen_resized",self,"resized")

func _ready():
	resized()

func resized():
	screen_size = OS.get_window_safe_area().size
