extends Node2D

onready var machine = $state_machine
onready var GUI = $gui
onready var fx_blue_bolt = preload("res://tools/fx/blue_topEffect.tscn").instance()

var WALK_SPD = 200


var dir_control

var path = null
var nav
var nav_rect = Rect2()

var cam_zoom = [1,1]

var walking =false
var anim = "idle"
var anim_new = "idle"

var y_scale = .2

var at_left = false#control for sprite horizontal flip switch

var face_dir = 0#current direction is facing


const dir_db = [
			["sd",false]#0			→
			,["fnsd",false]#1		↘
			,["fn",false]#2			↓
			,["fnsd",true]#3		↙ (#1 mirror)
			,["sd",true]#4			← (#0 mirror)
			,["bksd",true]#5		↖ (#7 mirror)
			,["bk",false]#6			↑
			,["bksd",false]#7		↗
]


func _enter_tree():
	$cam.current = true
	if nav == null:queue_free()
	set_nav_data()

func _ready():
	reparent()
	
func reparent():
	var orig = get_parent()
	var dest = get_node("../field")
	orig.remove_child(self)
	dest.add_child(self)
	self.set_owner(dest)



func shoot_fx():
	if path == null:return
	var shoot = fx_blue_bolt.duplicate()
	shoot.global_position = path[-1]+nav.global_position
	get_node("/root/main").add_child(shoot)
func set_nav_data():
	var polygon
	for i in nav.get_children():
		if i.get_class() == "NavigationPolygonInstance":
			polygon = i
	if polygon == null:queue_free()
	var navpoly = polygon.get_navigation_polygon()
# warning-ignore:unused_variable
	var navpos = nav.global_position
	for v in navpoly.get_vertices():
		if v.x < nav_rect.position.x or (nav_rect.position.x == 0):nav_rect.position.x = v.x
		if v.x > nav_rect.end.x or (nav_rect.end.x == 0):nav_rect.end.x = v.x
		if v.y < nav_rect.position.y or (nav_rect.position.y == 0):nav_rect.position.y = v.y
		if v.y > nav_rect.end.y or (nav_rect.end.y == 0):nav_rect.end.y = v.y
	if nav.get("cam_zoom"):
		cam_zoom = nav.cam_zoom
	if nav.get("y_scale"):
		y_scale = nav.y_scale
	if nav.get("WALK_SPD"):
		WALK_SPD = nav.WALK_SPD

	else:cam_zoom = [1,1]
	
func switch_anim():
	var to_left = dir_db[face_dir][1]
	if at_left != to_left:
# warning-ignore:unused_variable
		var sprite_scale = $sprite.scale.y
		$sprite.flip_h = to_left
		at_left = to_left
	$sprite.play(anim_new+"_"+dir_db[face_dir][0])
	anim=anim_new

func _update_navigation_path(start_position, end_position):
	var pos = nav.global_position
	start_position -= pos
	end_position -= pos
	var new_path = nav.get_simple_path(start_position, end_position, true)
	if (new_path[0]-new_path[-1]).length()>1:
		new_path.remove(0)
		path = new_path
	else:path = null

# warning-ignore:unused_argument
func _process(delta):
	$Label.text = str(machine.state,"\n",dir_control)

	dir_control = Vector2(Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),Input.get_action_strength("move_down") - Input.get_action_strength("move_up"))

	if anim != anim_new:switch_anim()

	return
# warning-ignore:unreachable_code
# warning-ignore:unused_variable
# warning-ignore:unreachable_code
	var value = inverse_lerp(nav_rect.position.y, nav_rect.end.y, position.y)


	if anim != anim_new:switch_anim()


func set_zoom(delta):
	var pos = inverse_lerp(nav_rect.position.y, nav_rect.end.y, position.y)
	var new_scale = lerp(cam_zoom[1],cam_zoom[0],pos)
	$sprite.scale = Vector2(new_scale,new_scale)
	return lerp(cam_zoom[1],cam_zoom[0],pos)


	var last_point = position
	var distance_between_points = last_point.distance_to(path[0])
	var distance = WALK_SPD*delta
	var direction = (path[0]-last_point).normalized()


	var y_speed = abs(direction.y)
	if y_scale != 1:
		var new_speed = 1-(y_scale*y_speed)
		distance = (WALK_SPD*new_speed)*delta


	
	position = last_point.linear_interpolate(path[0], distance / distance_between_points)
	if distance_between_points <=distance:
		if path.size()<2:
			walking = false
			anim_new = "idle"
			return
		path.remove(0)
	else:
		set_face_dir(direction)


func walk(delta):
	if path == null:return
	var last_point = position
	var distance_between_points = last_point.distance_to(path[0])
	var distance = WALK_SPD*delta
	var direction = (path[0]-last_point).normalized()


	var y_speed = abs(direction.y)
	if y_scale != 1:
		var new_speed = 1-(y_scale*y_speed)
		distance = (WALK_SPD*new_speed)*delta


	position = last_point.linear_interpolate(path[0], distance / distance_between_points)
	set_zoom(delta)
	if distance_between_points <=distance:
		if path.size()<2:
			return false
		path.remove(0)
	else:
		set_face_dir(direction)
	return true


func set_face_dir(direction = null):#0→||#1↘||#2↓||#3↙||#4←||#5↖||#6↑||#7↗||
	var new_face_dir = int( 8 * direction.angle() / (2*PI) + 8.5 ) % 8
	if new_face_dir != face_dir:
		face_dir = new_face_dir
		switch_anim()



#func move_along_path(distance):
#	var last_point = position
#	while path.size():
#		var distance_between_points = last_point.distance_to(path[0])
#
#		# the position to move to falls between two points
#		if distance <= distance_between_points:
#			position = last_point.linear_interpolate(path[0], distance / distance_between_points)
#			return
#
#		# the position is past the end of the segment
#		distance -= distance_between_points
#		last_point = path[0]
#		path.remove(0)
#	# the character reached the end of the path
#	position = last_point
#	walking = false
