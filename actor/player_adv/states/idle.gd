extends "res://actor/player_adv/states/state_base_adv.gd"


func logic(delta):
	owner.set_zoom(delta)
	if owner.dir_control != Vector2():

		var pos = owner.global_position+owner.dir_control*50
		owner._update_navigation_path(owner.global_position, pos)
		if owner.path != null:machine.set_state("walking")
	if Input.is_action_just_pressed("left_click"):
#		owner._update_navigation_path(owner.global_position, owner.get_global_mouse_position())
		owner._update_navigation_path(owner.global_position, owner.GUI.cursor.global_position)

		if owner.path != null:
			owner.shoot_fx()
			machine.set_state("walking")

#func handle_input(ev):
#	if ev.get_class() == "InputEventMouseButton":
#		owner._update_navigation_path(owner.global_position, owner.get_global_mouse_position())
#		if owner.path != null:
#			owner.shoot_fx()
#			machine.set_state("walking")
		

# warning-ignore:unused_argument
func entering(state_old):
	owner.anim_new = "walk2idle"

#func exiting(state_next):pass


func anim_finish(anim):
	if owner.anim == "walk2idle":
		owner.anim_new = "idle"
