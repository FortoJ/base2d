extends "res://actor/player_adv/states/state_base_adv.gd"





func logic(delta):
	if !owner.walk(delta):
		machine.set_state("idle")
		return
	if owner.dir_control != Vector2():
		var pos = owner.global_position+owner.dir_control*50
		owner._update_navigation_path(owner.global_position, pos)
	if Input.is_action_just_pressed("left_click"):
		owner._update_navigation_path(owner.global_position, owner.GUI.cursor.global_position)
		if owner.path != null:
			owner.shoot_fx()

	return
# warning-ignore:unreachable_code
	var last_point = owner.position
	var distance_between_points = last_point.distance_to(owner.path[0])
	var distance = owner.WALK_SPD*delta
	var direction = (owner.path[0]-last_point).normalized()


	var y_speed = abs(direction.y)
	if owner.y_scale != 1:
		var new_speed = 1-(owner.y_scale*y_speed)
		distance = (owner.WALK_SPD*new_speed)*delta

	owner.position = last_point.linear_interpolate(owner.path[0], distance / distance_between_points)
	if distance_between_points <=distance:
		if owner.path.size()<2:

#			owner.anim_new = "idle"
			machine.set_state("idle")
			return
		owner.path.remove(0)
	else:
		owner.set_face_dir(direction)




#func handle_input(ev):
#	if ev.get_class() == "InputEventMouseButton":
#		owner._update_navigation_path(owner.global_position, owner.get_global_mouse_position())
#		owner.shoot_fx()
#


# warning-ignore:unused_argument
func entering(state_old):
#	owner.anim_new = "walk"
	owner.anim_new = "idle2walk"
	

#func exiting(state_next):pass

func anim_finish(anim):
	if owner.anim == "idle2walk":
		owner.anim_new = "walk"
