extends KinematicBody2D



var velocity = Vector2()
const SPEED = 100
var dir_y_ratio = 0.5#if player walk/run slower or faster on y axis

var dir_control = Vector2()
var direction = Vector2()
var current_vel = Vector2()
var current_speed = 0#float that range between 0.0 (0) and 1.0 (max speed set by SPEED, ie: 500)

#"jump" mechanics

var height = .0#from 0.0 to 1.0 of pixel size of
var g_vel
const height_max = 200
const JUMP_PWR = 7
const G = 16
var sprite_orig#sprite original local position (since we'll change it later)


var anim = "idle"#control for animation switch
var anim_new = "idle"

var at_left = false
var to_left = false


func _enter_tree():
	$sprite.animation = "idlefight_sd"
	sprite_orig = $sprite.position

# warning-ignore:unused_argument
func _process(delta):
	#current speed
	current_vel = velocity#get velocity
	if dir_y_ratio != 1:#if y_ratio is not even
		current_speed/=dir_y_ratio#virtually adjust the velocity
	current_speed = min(1,current_vel.length()/SPEED)

	#get controls
	dir_control = Vector2(Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),Input.get_action_strength("move_down") - Input.get_action_strength("move_up"))
	dir_control.y *=dir_y_ratio#adjust by y_ratio



#	current_speed = velocity#get velocity
#	current_speed = min(1,current_speed.length()/SPEED)
#
#	#get controls
#	dir_control = Vector2(Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),Input.get_action_strength("move_down") - Input.get_action_strength("move_up"))

func _physics_process(delta):
	
	velocity = move_and_slide(velocity)
	
	var target_speed = direction*SPEED
	
	velocity = lerp(velocity, target_speed, 0.1)


	if Input.is_action_just_pressed("debug_a"):
		Engine.time_scale += 0.05
	if Input.is_action_just_pressed("debug_b"):
		Engine.time_scale -= 0.05
	if Input.is_action_just_pressed("debug_c"):
		height = 0.5
#		$sprite.position.y = -height_max
#	if Input.is_action_just_pressed("debug_d"):
#		anim_new = "walk"


	if anim != anim_new:
		$sprite.play(anim_new)
		anim=anim_new
	if at_left!=to_left:
		$sprite.scale.x = -$sprite.scale.x
		at_left = to_left

	if g_vel != null:gvel(delta)

func gvel(delta):
	g_vel +=G*delta
	var h = $sprite.position.y+g_vel
	if h >= sprite_orig.y:
		$sprite.position.y = sprite_orig.y
		g_vel = null
	else:
		$sprite.position.y = h

func push_anim(what, pose_as):
	$sprite.animation = what
	$sprite.frame = 0
	anim=pose_as
	anim_new=pose_as
