extends "res://actor/player_sidescroll/states/state_base.gd"

#var take_off = null
#var energy = 0
var dir = 0
# warning-ignore:unused_argument
func physics(delta):
	if dir != 0:
		owner.to_left = dir<0
	var gvel = owner.g_vel
	if gvel == null:
		machine.set_state("idle")
		return
	if gvel < 0:
		owner.anim_new = "fightjump3_sd"
	else:
		owner.anim_new = "fightfall2_sd"
	
	if Input.is_action_just_pressed("atk"):
		machine.set_state("air_atk")

#	return
#	var energy = owner.JUMP_PWR*(1-owner.height)
#	if take_off:
#		owner.height += owner.JUMP_PWR*(1-owner.height)
#		if energy <= 0.1:
#			take_off = false
#	else:
#		owner.anim_new = "fightjumpend_sd"
#		if owner.height <= 0:
#			owner.dir_control = Vector2()
#			owner.velocity = Vector2()
#			machine.set_state("idle")

#func logic(delta):return delta

#func handle_input(ev):return ev

func entering(state_old):
#	if !["idle","walk","air_atk"].has(state_old):return
	if ["idle","walk"].has(state_old):
		owner.anim_new = "fightjump3_sd"
		owner.g_vel = -owner.JUMP_PWR
		dir = owner.dir_control.x
		owner.direction.x = dir*2
		owner.direction.y = 0
	else:
		owner.anim_new = "fightfall2_sd"
		
#func exiting(state_next):pass

#func anim_finish(anim):pass

