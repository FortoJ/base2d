extends "res://actor/player_sidescroll/states/state_base.gd"

var timer

# warning-ignore:unused_argument
func physics(delta):
	if owner.g_vel == null:
		owner.direction = Vector2()
		owner.velocity = Vector2()
		machine.set_state("idle")
		

#func logic(delta):return delta

#func handle_input(ev):return ev

# warning-ignore:unused_argument
func entering(state_old):
	owner.anim_new = "flykick3_sd"

#func exiting(state_next):pass

# warning-ignore:unused_argument
func anim_finish(anim):
	if owner.anim != "flykick3_sd":return
	if timer != null:cleantime()
	timer = Timer.new()
# warning-ignore:return_value_discarded
	connect("finished",self,"timeout")
	

func timeout():
	pass

func cleantime():
	timer.queue_free()
	timer = null

func end_flykick():

	machine.set_state("jump")

