extends "res://actor/player_sidescroll/states/state_base.gd"

var mode = null

# warning-ignore:unused_argument
func physics(delta):
	owner.velocity = Vector2()

#func logic(delta):return delta

#func handle_input(ev):return ev

func entering(state_old):
	if ["idle","walk"].has(state_old):
		mode = "punch_1"
		owner.anim_new = "punch1_sd"
#		owner.velocity = Vector2()

# warning-ignore:unused_argument
func exiting(state_next):
	owner.velocity = Vector2()
#	owner.dir
# warning-ignore:standalone_expression
	mode == null

# warning-ignore:unused_argument
func anim_finish(anim):
	if mode != null:
		machine.set_state("idle")

