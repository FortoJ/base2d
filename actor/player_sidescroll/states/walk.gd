extends "res://actor/player_sidescroll/states/state_base.gd"

#double tap to dash (run) forward
var double_tap = null
var dt_timer = 0
const DT_TIME = 0.2

# warning-ignore:unused_argument
func physics(delta):
	if owner.current_speed <0.21:
			machine.set_state("idle")
			double_tap = null
			return
	var dir = owner.dir_control
	if dir.x == 0:owner.velocity.x *= 0.8
#	if double_tap == null:
#		if dir.x != 0:
#			double_tap = false
#	elif !double_tap:
#		if dir.x == 0:
#			double_tap = true
#			dt_timer = DT_TIME
#	else:
#		dt_timer -= delta
#		if dt_timer<=0:double_tap = null
#		if dir.x != 0:
#			machine.set_state("run")


	owner.direction = dir
	

# warning-ignore:unused_argument
func logic(delta):
	var dir = owner.dir_control.x
	
	if Input.is_action_just_pressed("atk"):
		if dir != 0:
			owner.to_left = dir <0
		machine.set_state("ground_atk")

	if Input.is_action_just_pressed("jump"):
		if dir != 0:
			owner.to_left = dir <0
		machine.set_state("jump")


	if owner.current_speed > 0.1 and (dir != 0):
		owner.to_left = (owner.velocity.x < 0)

#func handle_input(ev):return ev

# warning-ignore:unused_argument
func entering(state_old):
	owner.anim_new = "walkfight_sd"


#func exiting(state_next):pass

#func anim_finish(anim):pass

