extends Node
# warning-ignore:unused_signal
signal finished(next_state_name);
onready var machine = get_parent()

#func enter():return emit_signal("finished")

func physics(delta):return delta

func logic(delta):return delta

func handle_input(ev):return ev

# warning-ignore:unused_argument
func entering(state_old):pass

# warning-ignore:unused_argument
func exiting(state_next):pass

# warning-ignore:unused_argument
func anim_finish(anim):pass


