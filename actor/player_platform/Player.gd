extends KinematicBody2D


#const temp = 3

var velocity = Vector2()
var vel_target = Vector2()
const SPEED = 500#max speed
const G = 4000#gravity
const JUMP_PWR = 1300#jump power

var direction = Vector2()


var old_pos = Vector2()
var current_speed = 0
var h_speed = 0

var anim = "fall"
var anim_new = "fall"

var anim_frames

var at_left = false
var to_left = false

func _enter_tree():
# warning-ignore:return_value_discarded
	$sprite.connect("animation_finished",self,"animation_finished")
	anim_frames = $sprite.frames
	


func _physics_process(delta):
	velocity = move_and_slide(vel_target,Vector2(0,-1))
	current_speed = Vector2(max(0,min(1,abs(velocity.x)/SPEED)),velocity.y)

	direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	velocity.y += G*delta
	if get_slide_count() != 0:
		var body = get_slide_collision(get_slide_count()-1)
		if body.normal == Vector2(0,1):velocity.y = 0





# warning-ignore:unused_argument
func _process(delta):
	if Input.is_action_just_pressed("debug_a"):
		Engine.time_scale += 0.05
	if Input.is_action_just_pressed("debug_b"):
		Engine.time_scale -= 0.05
	if Input.is_action_just_pressed("debug_c"):
		$StateMachine.set_state("run")
	if Input.is_action_just_pressed("debug_d"):
		$StateMachine.set_state("jump")




	if anim != anim_new:
		$sprite.play(anim_new)
		anim=anim_new
	if at_left!=to_left:
		$sprite.flip_h=to_left
		at_left = to_left

#animation

#push anim: make an animation (what) act
#as another one (pose_as) to protect it
#ie: "run-stop" is considered "idle"
#so any request to switch into "idle"
#animation gets ignored
func push_anim(what, pose_as):
	$sprite.animation = what
	$sprite.frame = 0
	anim=pose_as
	anim_new=pose_as



#common phisics

func on_ground():return $feet.is_colliding()

func animation_finished():return
