extends "res://actor/player_platform/states/state_base.gd"
#extends "res://system/state_node.gd"

var fall_grace
var h_dir = 0#horizontal input direction synced between pyhisics and logic

#func _enter_tree():owner.get_node("sprite").connect("animation_finished",self,"anim_finish")

# warning-ignore:unused_argument
func logic(delta):
	var spd = owner.current_speed.x
	if spd > 0.1:
		owner.to_left = (owner.velocity.x < 0)

	if h_dir == 0:#
		owner.anim_new = "run-stop_sd"
	elif owner.at_left == (h_dir>0):
		owner.anim_new = "run-turn_sd"
	elif owner.anim != "run-turn_sd":
		owner.anim_new = "run_sd"

func physics(delta):
	h_dir = owner.SPEED*owner.direction.x
	var vel = owner.velocity

	if !owner.on_ground():
		if fall_grace == null:#missing ground, initiate grace period
			fall_grace = 0.07
		else:
			fall_grace -=delta
			vel.x *= 0.8
			if fall_grace<=0:
				machine.set_state("fall")
				return
	else:
		
		fall_grace=null
	if Input.is_action_just_pressed("jump"):
		vel.y = -owner.JUMP_PWR
		machine.set_state("jump")
	if Input.is_action_just_pressed("atk"):
		machine.action("punch")

	var h_vel = int(abs(vel.x))
	if h_vel <= 0:
		vel.x = 0
		machine.set_state("idle")
		return

	elif h_dir == 0:
		vel.x *= 0.75
	else:
		vel.x = machine.h_push(vel.x,h_dir)


#	owner.velocity.y += owner.gravity#*.5
	owner.vel_target = vel


# warning-ignore:unused_argument
func entering(state_old):
	owner.anim_new = "run_sd"
	
#func anim_finish(anim=null):
#	if owner.anim=="run-turn_sd":
#		owner.anim_new = "run_sd"
func anim_finish(anim):
	if anim=="run-turn_sd":
		owner.anim_new = "run_sd"
