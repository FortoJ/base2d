extends "res://actor/player_platform/states/state_base.gd"
#extends "res://system/state_node.gd"


var fall_grace

# warning-ignore:unused_argument
func logic(delta):
#	testing()
	owner.anim_new ="idle_sd"

func physics(delta):
	var vel = owner.velocity
	var h_dir = owner.SPEED*owner.direction.x

	if !owner.is_on_floor():
		if fall_grace == null:#missing ground, initiate grace period
			fall_grace = 0.4
		else:
			fall_grace -=delta
			if fall_grace<=0:
				machine.set_state("fall")
				return
	else:
		vel.y = 0
		fall_grace=null
		if Input.is_action_just_pressed("jump"):
			
			vel.y = -owner.JUMP_PWR
			machine.set_state("jump")
		if Input.is_action_just_pressed("atk"):
			machine.action("punch")

	#redo
	if owner.current_speed.x >= .1:
		machine.set_state("run")
	elif h_dir == 0:
		vel.x = 0
	else:
		vel.x = machine.h_push(vel.x,h_dir)
#		velocity.x = machine.push_speed(velocity,dir)

	owner.vel_target = vel

# warning-ignore:unused_argument
func entering(state_old):
	pass
#	print("idle was entered, before me: ",state_old)
	



# warning-ignore:unused_argument
func exiting(state_next):
	pass
#	print("idle was exit, before me: ",state_next)


