extends "res://actor/player_platform/states/state_base.gd"
#extends "res://system/state_node.gd"

var return_state = null
var action = null

# warning-ignore:unused_argument
func physics(delta):
	owner.velocity = Vector2()
	owner.vel_target = Vector2()



#func physics(delta):return delta
#
#func handle_input(ev):return ev
#
func entering(state_old):
	return_state = state_old
	if action == "punch":
		owner.anim_new = "hit-mid_sd"
		action = "hit-mid_sd"

# warning-ignore:unused_argument
func exiting(state_next):
# warning-ignore:standalone_expression
	action == null

# warning-ignore:unused_argument
func anim_finish(anim):
	if action == null:return
	machine.set_state(return_state)
