extends "res://actor/player_platform/states/state_base.gd"
#extends "res://system/state_node.gd"

var jump_boost = false

# warning-ignore:unused_argument
func physics(delta):
	var h_dir = owner.SPEED*owner.direction.x
	var vel = owner.velocity
	if owner.current_speed.y >= 0:
		owner.anim_new="fall_sd"
		machine.set_state("fall")
#		vel.y = -owner.G
		return

	if owner.is_on_floor():
		machine.set_state("run")

	
	
	if jump_boost:
		if !Input.is_action_pressed("jump"):
			jump_boost = false

		vel.y-=owner.G*0.006#a small chunk of gravity is removed while [jump] is pressed (jump higher)
		
		
	
	if h_dir == 0:
		vel.x *=0.91
	else:
		vel.x = machine.h_push(vel.x,h_dir,true)
#		velocity.x = lerp(velocity.x, dir,delta*owner.temp)
#		velocity.x = machine.push_speed(velocity,dir)




	owner.vel_target = vel


#func physics(delta):return delta
#
#func handle_input(ev):return ev
#
func entering(state_old):
#	print("from:", state_old)
	jump_boost = true
	if ["idle","run","walk"].has(state_old):owner.anim_new = "jump_sd"
	var dir = owner.direction.x
	if dir != 0:
		owner.to_left = dir <0

#
# warning-ignore:unused_argument
func exiting(state_next):
#	print("exit to:",state_next)
	jump_boost = false
