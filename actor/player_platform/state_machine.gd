extends Node
#extends "res://system/state_machine.gd"





var state = null setget set_state
var state_old = null

var state_db = {}
onready var parent = get_parent()

#initiate machine
func _add_state(state_add):
	state_db[state_add] = get_node(state_add)
	
func _enter_tree():
# warning-ignore:return_value_discarded
	owner.get_node("sprite").connect("animation_finished",self,"anim_finished")
	
func _ready():
	for i in get_children():
		_add_state(i.name)
	call_deferred("set_state","fall")


#processes
func _input(event):
	if state != null:
		state_db[state].handle_input(event)

func _physics_process(delta):
	if state != null:state_db[state].physics(delta)

func _process(delta):
	get_node("../stateinfo").text = str(state,"\n",owner.current_speed)
	if state != null:state_db[state].logic(delta)

func anim_finished():
	if state != null:state_db[state].anim_finish(owner.anim)


#switcher
func set_state(new_state):
#	print("new state: ",new_state)
	if state == new_state:return
	state_old = state
	state = new_state

	if ![state_old,state].has(null):
		state_db[state].entering(state_old)
		state_db[state_old].exiting(state)




#mechanics helper

func h_push(vel,h_dir,air=false):
#	h_dir*=owner.SPEED
	
	if air:
		if (h_dir<0) == owner.at_left:
			return lerp(vel, h_dir,.2)
		else:
			return lerp(vel, h_dir,.1)
	else:
		return lerp(vel, h_dir,.5)


func action(which):
	state_db["action"].action = which
	set_state("action")
	pass
