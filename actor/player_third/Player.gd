extends KinematicBody2D



var velocity = Vector2()
const SPEED = 400
const WALK_SPD = 0.161#range between WALK~RUN is considered walk
const RUN_SPD = 0.5# 0.5 = half and beyond max speed we consider the actor running

var direction = Vector2()#root direction actor will move
var dir_control = Vector2()#directional key inputs
var dir_y_ratio = 0.5#if player walk/run slower or faster on y axis (ie: isometric games like Flare require 0.5 to blend Y movment with the background)

var current_speed = 0#float that range between 0.0 (0) and 1.0 (max speed set by SPEED, ie: 500)


var anim = "idle"#control for animation switch
var anim_new = "idle"

var at_left = false#control for sprite horizontal flip switch

var face_dir = 0#current direction is facing

#dictionary for the 8-way animation and if it's mirrored
#ie: anim "walk" while facing right (→ #0) become "walk_sd" not mirrored
#idle while facing up left (↖ #5) become "idle_bksd" mirrored
const dir_db = [
			["sd",false]#0			→
			,["fnsd",false]#1		↘
			,["fn",false]#2			↓
			,["fnsd",true]#3		↙ (#1 mirror)
			,["sd",true]#4			← (#0 mirror)
			,["bksd",true]#5		↖ (#7 mirror)
			,["bk",false]#6			↑
			,["bksd",false]#7		↗
]


#func _enter_tree():
#	$sprite.connect("animation_finished",self,"animation_finished")
#	anim_frames = $sprite.frames

func _ready():
	anim = ""
	anim_new = "idle_sd"

# warning-ignore:unused_argument
func _process(delta):
	
	#current speed
	current_speed = velocity#get velocity
	if dir_y_ratio != 1:#if y_ratio is not even
		current_speed/=dir_y_ratio#virtually adjust the velocity
	current_speed = min(1,current_speed.length()/SPEED)

	#get controls
	dir_control = Vector2(Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),Input.get_action_strength("move_down") - Input.get_action_strength("move_up"))
	dir_control.y *=dir_y_ratio#adjust by y_ratio

# warning-ignore:unused_argument
func _physics_process(delta):
	
	velocity = move_and_slide(velocity)
	
	var target_speed = direction*SPEED
	
	velocity = lerp(velocity, target_speed, 0.1)


	if Input.is_action_just_pressed("debug_a"):
		Engine.time_scale += 0.05
	if Input.is_action_just_pressed("debug_b"):
		Engine.time_scale -= 0.05
#	if Input.is_action_just_pressed("debug_c"):
#		anim_new = "idle"
#	if Input.is_action_just_pressed("debug_d"):
#		anim_new = "walk"



	if anim != anim_new:switch_anim()


#animation
func switch_anim():
	var to_left = dir_db[face_dir][1]
	if at_left != to_left:
		var sprite_scale = $sprite.scale.y
		if to_left:	$sprite.scale.x = -sprite_scale
		else:$sprite.scale.x = sprite_scale
		at_left = to_left
	$sprite.play(anim_new+"_"+dir_db[face_dir][0])
	anim=anim_new

func push_anim(what, pose_as):
	$sprite.animation = what
	$sprite.frame = 0
	anim=pose_as
	anim_new=pose_as

# warning-ignore:shadowed_variable
func set_face_dir(direction = null):#0→||#1↘||#2↓||#3↙||#4←||#5↖||#6↑||#7↗||
	if direction == null:direction=velocity
	var new_face_dir = int( 8 * direction.angle() / (2*PI) + 8.5 ) % 8
	if new_face_dir != face_dir:
		face_dir = new_face_dir
		switch_anim()

