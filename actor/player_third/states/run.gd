extends "res://actor/player_third/states/state_base.gd"

var sprite

const ANIM_SCALE = [0.3,1]
func _enter_tree():
	sprite = owner.get_node("sprite")

# warning-ignore:unused_argument
func physics(delta):

	if owner.current_speed <owner.WALK_SPD:
		machine.set_state("idle")
		return
	elif owner.current_speed <owner.RUN_SPD:
		machine.set_state("walk")
		return
	var direction = owner.dir_control


	owner.direction = direction
	owner.set_face_dir()
	if direction == Vector2():
		owner.velocity *= 0.9
#func logic(delta):return delta

#func handle_input(ev):return ev

# warning-ignore:unused_argument
func logic(delta):
	var run =min(1,max(0,inverse_lerp(owner.WALK_SPD,owner.RUN_SPD, owner.current_speed)))
	sprite.speed_scale = lerp(ANIM_SCALE[0],ANIM_SCALE[1],run)


# warning-ignore:unused_argument
func entering(state_old):
	owner.anim_new = "run"

# warning-ignore:unused_argument
func exiting(state_next):
	sprite.speed_scale = 1
#func exiting(state_next):pass

#func anim_finish(anim):pass

