extends "res://actor/player_third/states/state_base.gd"

var sprite

const ANIM_SCALE = [0.3,1]
func _enter_tree():
	sprite = owner.get_node("sprite")
	
# warning-ignore:unused_argument
func physics(delta):

	if owner.current_speed <owner.WALK_SPD:
		machine.set_state("idle")
		return
	if owner.current_speed >owner.RUN_SPD:
#		print("more than")
		machine.set_state("run")
		return
	var direction = owner.dir_control
	if direction == Vector2():
		owner.velocity *= 0.9
		owner.anim_new = "run-stop"
	else:
		owner.anim_new = "walk"
	owner.direction = direction
	owner.set_face_dir()

# warning-ignore:unused_argument
func logic(delta):
	var walk =max(0,inverse_lerp(owner.WALK_SPD,owner.RUN_SPD, owner.current_speed))
	sprite.speed_scale = lerp(ANIM_SCALE[0],ANIM_SCALE[1],walk)

		
#	print(owner.current_speed/owner.RUN_SPD)

#func handle_input(ev):return ev

# warning-ignore:unused_argument
func entering(state_old):
	owner.anim_new = "walk"
#	sprite.speed_scale = ANIM_SCALE[0]

# warning-ignore:unused_argument
func exiting(state_next):
	sprite.speed_scale = 1

#func anim_finish(anim):pass

