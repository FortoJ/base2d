extends "res://actor/player_third/states/state_base.gd"

var sprite

func _enter_tree():
	sprite = owner.get_node("sprite")


# warning-ignore:unused_argument
func physics(delta):
	if owner.current_speed >owner.WALK_SPD:
		machine.set_state("walk")
		return
#	var direction = Vector2(Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),Input.get_action_strength("move_down") - Input.get_action_strength("move_up"))
	owner.direction = owner.dir_control

# warning-ignore:unused_argument
func logic(delta):
	if owner.anim != "run-top":owner.anim_new = "idle"
	elif !sprite.is_playing():owner.anim_new = "idle"
#func handle_input(ev):return ev

#func entering(state_old):
#	owner.anim_new = "idle"

#func exiting(state_next):pass

#func anim_finish(anim):pass

