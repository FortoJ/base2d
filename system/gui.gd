extends Control


func _enter_tree():
# warning-ignore:return_value_discarded
	$over/platformer.connect("pressed",self,"run_platform")
# warning-ignore:return_value_discarded
	$over/isometric.connect("pressed",self,"run_isometric")
# warning-ignore:return_value_discarded
	$over/scroller.connect("pressed",self,"run_scroller")
# warning-ignore:return_value_discarded
	$over/adventure.connect("pressed",self,"run_adventure")


func run_adventure():
	get_node("/root/main").run_adventure()
func run_platform():
	get_node("/root/main").run_platformer()
func run_isometric():
	get_node("/root/main").run_isometric()
#	get_parent().get_parent().queue_free()


func run_scroller():
	get_node("/root/main").run_scroller()
