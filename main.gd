extends Node2D

var platformer_actor = preload("res://actor/player_platform/Player.tscn")
var isometric_actor = preload("res://actor/player_third/Player.tscn")
var scroller_actor = preload("res://actor/player_sidescroll/Player.tscn")
var adventure_actor = preload("res://actor/player_adv/player_adv.tscn")

var gui = preload("res://system/gui.tscn")

var current_player = null
func _enter_tree():
# warning-ignore:return_value_discarded
	$area_switch_toiso.connect("body_entered",self,"area_switch_toiso")
# warning-ignore:return_value_discarded
	$area_switch_toplat.connect("body_entered",self,"area_switch_toplat")
# warning-ignore:return_value_discarded
	$area_switch_scrolltoiso.connect("body_entered",self,"area_switch_scrolltoiso")
# warning-ignore:return_value_discarded
	$area_switch_scroll.connect("body_entered",self,"area_switch_scroll")
# warning-ignore:return_value_discarded
	$area_switch_toadv.connect("body_entered",self,"area_switch_toadv")
# warning-ignore:return_value_discarded
	$area_adv_switch_toiso.connect("area_entered",self,"area_switch_adv2iso")

# warning-ignore:unused_argument
func _process(delta):
	if Input.is_action_just_pressed("debug_d"):
		var shot = load("res://tools/fx/blue_topEffect.tscn").instance()
		shot.global_position = get_global_mouse_position()
		add_child(shot)



func _ready():
#	run_adventure()
	run_platformer()
#	run_scroller()

func area_switch_scrolltoiso(who):
	if who.get_meta("kind") != "scroller":return
	$wall/spawn.global_position = $area_switch_scrolltoiso/spawn.global_position
	run_isometric()
func area_switch_scroll(who):
	if who.get_meta("kind") != "isometric":return
	run_scroller()

func area_switch_adv2iso(who):
	if who.owner.get_meta("kind") != "adventure":return
	$wall/spawn.global_position = $area_switch_toadv.global_position
	run_isometric()


func area_switch_toiso(who):
	if who.get_meta("kind") != "platformer":return
	$wall/spawn.global_position = who.global_position
	run_isometric()

func area_switch_toplat(who):
	if who.get_meta("kind") != "isometric":return
	$platformer/spawn.global_position = who.global_position
	run_platformer()


func area_switch_toadv(who):
	if who.get_meta("kind") != "isometric":return
#	$platformer/spawn.global_position = who.global_position
	run_adventure()


func run_platformer():
	if current_player != null:
		current_player.queue_free()
	current_player = platformer_actor.instance()
	current_player.global_position = $platformer/spawn.global_position
	current_player.set_meta("kind","platformer")
	$platformer.add_child(current_player)
	
	var new_gui = gui.instance()
	current_player.get_node("cam").add_child(new_gui)


func run_scroller():
	if current_player != null:
		current_player.queue_free()
	current_player = scroller_actor.instance()

	$sidescroll.add_child(current_player)
	current_player.global_position = $sidescroll/spawn.global_position
	current_player.set_meta("kind","scroller")
	var new_gui = gui.instance()
	current_player.get_node("cam").add_child(new_gui)
#	current_player.get_node("cam").call_deferred("set_zoom",[Vector2(.5,.5)])
	current_player.get_node("cam").zoom = Vector2(.5,.5)


func run_adventure():
	if current_player != null:
		current_player.queue_free()
	current_player = adventure_actor.instance()
	current_player.set_meta("kind","adventure")
	var room = $adventure
	current_player.nav = room.get_node("nav")
	room.add_child(current_player)
	current_player.global_position = room.get_node("spawn/entry_a").global_position

	var new_gui = gui.instance()
	current_player.get_node("cam").add_child(new_gui)


func run_isometric():
	if current_player != null:
		current_player.queue_free()
	current_player = isometric_actor.instance()

	$wall.add_child(current_player)
	current_player.global_position = $wall/spawn.global_position
	current_player.set_meta("kind","isometric")
	var new_gui = gui.instance()
	current_player.get_node("cam").add_child(new_gui)
#	current_player.get_node("cam").zoom = Vector2(.5,.5)
#	current_player.get_node("cam").call_deferred("set_zoom",[Vector2(.5,.5)])
	
